from application import db
from sqlalchemy import Column, ForeignKey, Integer, String, Date, Boolean
from sqlalchemy import UniqueConstraint
from sqlalchemy.event import listens_for
from sqlalchemy.orm import relationship
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash


class User(UserMixin, db.Model):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(String(100), nullable=False, unique=True)
    email = Column(String(250), nullable=True, unique=True, index=True)
    password_hash = Column(String(128), nullable=True)
    # role = Column(String(250), nullable=True, unique=False)

    # Hashing functions
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    # Show readable user name instead of id
    def __repr__(self):
        return self.username


class CollectionType(db.Model):
    __tablename__ = 'collection_type'
    id = Column(Integer, nullable=False, primary_key=True)
    name = Column(String(250), nullable=False)
    icon_class = Column(String(250), nullable=True)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=True)
    user = relationship(User)

    # Show readable name instead of id
    def __repr__(self):
        return self.name


@listens_for(CollectionType.__table__, 'after_create')
def insert_initial_values(*args, **kwargs):
    db.session.add(CollectionType(name='Books', icon_class="fa-book"))
    db.session.add(CollectionType(name='Games', icon_class="fa-gamepad"))
    db.session.add(CollectionType(name='Media', icon_class="fa-film"))
    db.session.add(User(username='Pooka', email='ilaria@ilaria.com',
                        password_hash='pbkdf2:sha256:150000$Ggf0mvou$' +
                        'babf18f63b3b829ca85ff51d7ea8267eac1c766e4e7b' +
                        'd163bc8395957ead183b'))
    db.session.add(User(username='Kevin', email='kevin@kevin.com',
                        password_hash='pbkdf2:sha256:150000$Ggf0mvou$' +
                        'babf18f63b3b829ca85ff51d7ea8267eac1c766e4e7b' +
                        'd163bc8395957ead183b'))
    db.session.add(Collection(name='MyGames', collection_type_id=2, user_id=1))
    db.session.add(Collection(name='MyBooks', collection_type_id=1, user_id=1))
    db.session.add(Collection(name='PS', collection_type_id=2, user_id=2))
    db.session.add(Collection(name='Coding', collection_type_id=1, user_id=2))
    db.session.add(Item(name='Mario Cart8', subtype='Switch', collection_id=1))
    db.session.add(Item(name='Fiori Blu', subtype='Prefe', collection_id=2))
    db.session.add(Item(name='No mans sky', subtype='PS', collection_id=3))
    # db.session.add(Item(name='', subtype='', collection_id=1))
    db.session.commit()


class Collection(db.Model):
    __tablename__ = 'collection'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    collection_type_id = Column(Integer,
                                ForeignKey('collection_type.id'),
                                nullable=False)
    collection_type = relationship(CollectionType)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship(User)
    __table_args__ = (UniqueConstraint('name', 'user_id'),)

    def icon_class(self):
        return self.collection_type.icon_class

    # Show readable name instead of id
    def __repr__(self):
        return self.name


class Item(db.Model):
    __tablename__ = 'item'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    subtype = Column(String(500), nullable=True)
    description = Column(String(5000), nullable=True)
    picture = Column(String(250), nullable=True)
    upc = Column(Integer, nullable=True, unique=True)
    current_owner = Column(Integer, ForeignKey('user.id'), nullable=True)
    lent_date = Column(Date, nullable=True)
    expected_return_date = Column(Date, nullable=True)
    location = Column(String(250), nullable=True)
    collection_id = Column(Integer,
                           ForeignKey('collection.id'),
                           nullable=False)
    collection = relationship(Collection)

    # Show readable name instead of id
    def __repr__(self):
        return self.name
