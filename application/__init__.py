from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required
from flask_bootstrap import Bootstrap


# Create database connection object
db = SQLAlchemy()


def create_app():
    '''Create app'''
    app = Flask(__name__)
    bootstrap = Bootstrap(app)
    app.config.from_object('config.Config')

    # Initialize Plugins
    db.init_app(app)

    with app.app_context():
        # Include our Routes
        from . import routes

        # Register Blueprints
        # app.register_blueprint(auth.auth_bp)
        # app.register_blueprint(admin.admin_bp)

        db.create_all()

        return app
