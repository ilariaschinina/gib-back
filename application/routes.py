import flask
from flask import current_app as app
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_security import Security, login_required
from flask_login import LoginManager, login_user, current_user, logout_user
from sqlalchemy import func
from sqlalchemy.exc import IntegrityError
from .models import User, Collection, Item, CollectionType
from werkzeug.security import generate_password_hash, check_password_hash
from . import db
from . import forms as my_forms


session = db.session

# Admin database views
admin = Admin(app, name='Gib Back', template_mode='bootstrap3')
admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Collection, db.session))
admin.add_view(ModelView(Item, db.session))
# admin.add_view(ModelView(Post, db.session))

# Views
@app.route("/")
def home():
    # user_id = current_user.get_id()
    # if user_id:
    #     return flask.redirect(flask.url_for('my_collections'))
    return flask.render_template('index.html')


@app.route("/users")
def users():
    users = session.query(User).all()
    return users


# Configuring LoginManager which lets Flask-Login and the app work together
login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.query.filter_by(id=user_id).first()


@app.route("/signup", methods=['GET', 'POST'])
def signup():
    user_id = current_user.get_id()
    if user_id:
        return flask.redirect(flask.url_for('my_collections'))
    form = my_forms.SignupForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flask.flash('Congratulations, now you can create your collections!')
        return flask.redirect(flask.url_for('my_collections'))

    return flask.render_template('signup.html', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    user_id = current_user.get_id()
    if user_id:
        return flask.redirect(flask.url_for('my_collections'))
    form = my_forms.LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            if check_password_hash(user.password_hash, form.password.data):
                login_user(user)
                return flask.redirect(flask.url_for('my_collections'))
            else:
                form.password.errors.append('Incorrect password')
        else:
            form.email.errors.append('Incorrect e-mail')
    return flask.render_template('login.html', form=form)


@app.route("/settings")
# Any crud function
@login_required
def settings():
    pass


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return flask.redirect("/")


@app.route("/collection/list")
@login_required
def my_collections():
    user_id = current_user.get_id()
    collections = session.query(Collection).filter_by(user_id=user_id).\
        add_column(func.count(Item.name)).\
        outerjoin(Item).group_by(Collection.id).all()
    return flask.render_template('collection_list.html',
                                 collections=collections)


@app.route("/collection/<int:collection_id>")
@login_required
def my_items_by_cat(collection_id):
    collection = session.query(Collection).filter_by(id=collection_id).one()
    items = session.query(Item).filter_by(collection_id=collection_id).all()
    return flask.render_template('collection.html',
                                 items=items,
                                 collection=collection)


@app.route("/collection/new", methods=['GET', 'POST'])
@login_required
def add_collection():
    form = my_forms.AddCollection()
    form.collection_type.choices = [(ct.id, ct.name) for ct in session.
                                    query(CollectionType).order_by('name')]
    if form.validate_on_submit():
        user_id = current_user.get_id()
        collection = Collection()
        if form.name.data:
            collection.name = form.name.data
        if form.collection_type.data:
            collection.collection_type_id = form.collection_type.data
        collection.user_id = user_id
        session.add(collection)
        try:
            session.commit()
            flask.flash('"%s" has been successfully added' % collection.name)
            return flask.redirect(flask.url_for('my_collections'))
        except IntegrityError:
            form.name.errors.append('A collection named "%s" already exists'
                                    % collection.name)
    return flask.render_template('add_collection.html', form=form)


@app.route("/collection/<int:collection_id>/edit",
           methods=['GET', 'POST'])
@login_required
def edit_collection(collection_id):
    collection = session.query(Collection).\
                filter_by(id=collection_id).one()
    form = my_forms.EditCollection()
    form.collection_type.choices = [(ct.id, ct.name) for ct in session.
                                    query(CollectionType).order_by('name')]
    # form.collection_type.choices = [(ct.id, ct.name) for ct in session.
    #                                 query(CollectionType).order_by('name')]
    if form.validate_on_submit():
        user_id = current_user.get_id()
        if form.name.data:
            collection.name = form.name.data
        if form.collection_type.data:
            collection.collection_type_id = form.collection_type.data
        collection.user_id = user_id
        session.add(collection)
        try:
            session.commit()
            flask.flash('Your changes have been saved')
            return flask.redirect(flask.url_for('my_collections',
                                  collection_id=collection_id))
        except IntegrityError:
            form.name.errors.append('A collection named "%s" already exists'
                                    % collection.name)
    return flask.render_template('edit_collection.html', form=form,
                                 collection=collection)


@app.route("/collection/<int:collection_id>/item/new", methods=['GET', 'POST'])
@login_required
def add_item(collection_id):
    collection = session.query(Collection).\
                filter_by(id=collection_id).one()
    form = my_forms.AddItem()
    if form.validate_on_submit():
        item = Item()
        if form.name.data:
            item.name = form.name.data
        if form.subtype.data:
            item.subtype = form.subtype.data
        if form.description.data:
            item.description = form.description.data
        if form.upc.data:
            item.upc = form.upc.data
        if form.location.data:
            item.location = form.location.data
        item.collection_id = collection.id
        session.add(item)
        try:
            session.commit()
            flask.flash('"%s" has been successfully added' % item.name)
            return flask.redirect(flask.url_for('my_items_by_cat',
                                  collection_id=collection_id))
        except IntegrityError:
            form.name.errors.append('An item named "%s" already exists'
                                    % item.name)
    return flask.render_template('add_item.html', form=form,
                                 collection=collection)


@app.route("/collection/<int:collection_id>/item/<int:item_id>/edit",
           methods=['GET', 'POST'])
@login_required
def edit_item(collection_id, item_id):
    collection = session.query(Collection).\
                filter_by(id=collection_id).one()
    item = session.query(Item).filter_by(id=item_id).one()
    form = my_forms.EditItem()
    # form.collection_type.choices = [(ct.id, ct.name) for ct in session.
    #                                 query(CollectionType).order_by('name')]
    if form.validate_on_submit():
        # I need the collection_id here!!!
        if form.name.data:
            item.name = form.name.data
        if form.subtype.data:
            item.subtype = form.subtype.data
        if form.description.data:
            item.description = form.description.data
        # if form.picture.data:
        #     item.picture = form.picture.data
        if form.upc.data:
            item.upc = form.upc.data
        # if form.current_owner.data:
        #     item.current_owner = form.current_owner.data
        # if form.lent_date.data:
        #     item.lent_date = form.lent_date.data
        # if form.expected_return_date.data:
        #     item.expected_return_date = form.expected_return_date.data
        if form.location.data:
            item.location = form.location.data
        item.collection_id = collection.id
        session.add(item)
        try:
            session.commit()
            flask.flash('Your changes have been saved')
            return flask.redirect(flask.url_for('my_items_by_cat',
                                  collection_id=collection_id))
        except IntegrityError:
            form.name.errors.append('An item named "%s" already exists'
                                    % item.name)
    return flask.render_template('edit_item.html', form=form,
                                 collection=collection, item=item)
