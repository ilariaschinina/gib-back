from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms import SelectField, DateField, IntegerField
from wtforms.validators import ValidationError, DataRequired, Email
from wtforms.validators import EqualTo, Length, Optional
from werkzeug.security import generate_password_hash
from .models import User, Collection, Item


class LoginForm(FlaskForm):
    email = StringField('E-mail', [DataRequired()])
    password = PasswordField('Password', [DataRequired()])
    # remember = BooleanField('Remember me')
    submit = SubmitField('Submit')


class SignupForm(FlaskForm):
    username = StringField('Username', [DataRequired()])
    email = StringField('E-mail', [DataRequired()])
    password = PasswordField('Password', [DataRequired(), Length(min=8,
                             message="At least 8 characters long")])
    password2 = PasswordField('Repeat Password', [
        DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('This username is already taken')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('This email has been already registerd')


class AddCollection(FlaskForm):
    name = StringField('Collection Name', [DataRequired()])
    collection_type = SelectField('Collection Type', coerce=int)
    submit = SubmitField('Create')


class EditCollection(FlaskForm):
    name = StringField('Collection Name', [DataRequired()])
    collection_type = SelectField('Collection Type', coerce=int)
    submit = SubmitField('Save')


class AddItem(FlaskForm):
    name = StringField('Name', [DataRequired()])
    subtype = StringField('Subtype')
    description = StringField('Description')
    # picture = StringField('Picture')
    upc = IntegerField('UPC', [Optional()])
    # current_owner = StringField('Current Owner')
    location = StringField('Location')
    submit = SubmitField('Create')


class EditItem(FlaskForm):
    name = StringField('Name', [DataRequired()])
    subtype = StringField('Subtype')
    description = StringField('Description')
    # picture = StringField('Picture')
    upc = IntegerField('UPC', [Optional()])
    # current_owner = StringField('Current Owner')
    location = StringField('Location')
    submit = SubmitField('Save')

    # name = Column(String(250), nullable=False)
    # subtype = Column(String(500), nullable=True)
    # description = Column(String(5000), nullable=True)
    # picture = Column(String(250), nullable=True)
    # upc = Column(Integer, nullable=True, unique=True)
    # current_owner = Column(Integer, ForeignKey('user.id'), nullable=True)
    # lent_date = Column(Date, nullable=True)
    # expected_return_date = Column(Date, nullable=True)
    # location = Column(String(250), nullable=True)
    # collection_id = Column(Integer,
    #                        ForeignKey('collection.id'),
    #                        nullable=False)
    # collection = relationship(Collection)
